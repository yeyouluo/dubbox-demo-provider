package com.yeyouluo.dubbox.provider.inter;

import java.util.List;

import com.yeyouluo.dubbox.provider.pojo.User;

public interface IDemoService {

	public String sayHello(String name);
	
	public List<User> getUsers();
}
