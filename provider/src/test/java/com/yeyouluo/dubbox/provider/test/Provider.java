package com.yeyouluo.dubbox.provider.test;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Provider {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("dubbox-provider.xml");
		ac.start();
		
		// 为保证服务一直开着，利用输入流的阻塞来模拟 
		System.in.read();
	}

}
